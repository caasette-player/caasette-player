# `@caasette-player/commitments`

(Copied from https://github.com/emberjs/rfcs)

> The contracts that an implimentation is expected to have.

![](https://i.imgur.com/3wZ0Mdb.jpg)

`caasette-player` isn't the concrete implimentation of a `caasette-player` it's the contract that any implimentation should require. Possibly a CLI to create an initial repo as well?

```
  $caasette-player-name
    - Branding
    - Dependancies (or use the one from caasette-player)
    - Style guide
    - The use of "caasette-wells"
```

# Documentation
* [How to run your guild](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments/documentation/guild.md)
* [The seemingly convoluted naming scheme of Caasette Player and why you're wrong about that. Champ.](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments/documentation/caasette-player.md)
* [Architecture](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments/documentation/architecture.md)
* [Why UMDs](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments/documentation/caasette-umd-export.md)
* [Juice boxes. No, I mean Jukeboxes](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments/documentation/Jukebox.md)
* [The Caasette Deck](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments/documentation/caasette-deck.md)
* [Caasette me up scotty!](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments/documentation/caasette.md)
* Workflows
    * [Engineer](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments/documentation/workflow-dev.md)
    * [Organization](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments/documentation/workflow-organization.md)
