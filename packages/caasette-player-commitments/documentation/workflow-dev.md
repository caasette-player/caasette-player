# Organization Workflow

## Summary
How dev would impliment `caasette-player` and how to setup your dev sandbox to build things out.

## Motivation
I believe that building a component should be simple and fast. <br />
I believe that building a component should have it's own sandbox. <br />
I believe that configuration is the devil and more of it that I do not have to do the better. <br />
I believe that documentation should be written inline and published for me. <br />
I believe that documentation should be written wherever and however I like. <br />
I believe that the system should not get in my way but I'm willing to deal with some issues. <br />
I believe that I should have autonomy as to how I build my software. <br />
I believe that disparate code should still be able to work together. <br />
I believe that I should be able to compile my documentation together from many different repos. <br />
I believe that my business rules should be easy to write and file away for later use. <br />
I believe that writing tests should not get in my way. <br />

These are all things that I believe are important to writing not only good components but good code. This, I believe, is a great way to achieve those end goals. Using `blank-tape` and the `caasette-player` tools we can build an eco-system to suit our needs and make it easy to build all our code.

## Detailed design
No.

## How we teach this
Use the CLI tools or pull in `blank-tape` and get started. 

## Drawbacks
There really shouldn't be any here. This is straightforward and simple.

## Alternatives

## Unresolved questions
