# CAASETTEs, UMDs & You

## Summary
A `caasette` is a particular [UMD module](https://github.com/umdjs/umd).

## Motivation
The reasoning behind making each `caasette` a "Universal Module Definition" is so that we can pull in code from the cloud and each module is wrapped thus not polluting the global namespace. This allows us more control over the existance of the module to send events to and from it while allowing us to remove it if required for memory reasons.

## Detailed design
This will be short. The rollup configuration looks [more-or-less] like the example below. It also uses our custom `rollup-babel` plugin that allows us to modify the export to add the manifest (more on that in another document).

```js
// File: `rollup.dev.js`
export default {
  output: {
    name: `@caasette.${caasetteName}`,
    file: path.resolve(dist, `${caasetteName}.js`),
    format: 'umd',
    exports: 'named',
    sourcemap: 'inline',
    globals: {
      styled: 'styled-components',
      react: 'React',
      'react-dom': 'ReactDOM',
    },
  },
  plugins: [
    ...base.plugins,
  ],
}
```

## Drawbacks
People don't like "old" techs. Cuz they're fuddy duddies.

## Alternatives
Use `System.js` and any type of module setup but there are a couple of drawbacks here which is why I chose UMD as the module definition.
1. `Common.js` is not wrapped.
1. `AMD` is just awful to work with at this point. 
1. Need to export the manifest alongside the `caasette`.
1. Need to add to `window['@caasettes]` variable so that we can manage each `caasette`.
1. Adding `System.js` is yet another dependancy and this JS could be a lot smaller.

## Unresolved questions
The rollup babel plugin.
