# Engineer Benefits

## Summary
An engineer should be able to build a `@caasette` with relative ease and be able to export that `@caasette` to the world w/o issues while generating it's documentation adhoc and an existing zero-config dev sandbox should exist.

## Motivation
As an engineer I find getting started to be incredibly tedious and fault ridden that I tend to either use something that already exists (boilerplate) or hack my way through to the end (since documentation usually shows happy path and not the more realistic path). 

What if, instead, you could build a repo complete with everything required to make at minimum 1 `@caasette` and build everything required?

## But how?
Using `@caasette-player/blank-tape` we can build ourselves a repo that contains everything required to make a `@caasette`.

* Uses [`docz`](https://www.docz.site) For the dev sandbox
* Uses [`mdx`](https://github.com/mdx-js/mdx) for the dev sandbox and to write documentation
* Uses [`cypress.io`](https://www.cypress.io/) for end-to-end testing and documenting (FE) business rules.
* Uses [`rollup`](https://rollupjs.org/guide/en) to output a library
* Uses [`lerna`](https://github.com/lerna/lerna) to make linked packages.
* Uses `@caasette-player/umd` to export a rollup component with the manifest attached.
