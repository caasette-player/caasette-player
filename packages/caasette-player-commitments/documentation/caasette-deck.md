# The Caasette Deck

## Summary
A `caasette-deck` is an instance of the `caasette-player` definition. `@caasette-player/deck` is our implimentation of a deck that anyone may use.

## Detailed design
A `caasette-deck` isn't complicated, really, as any web application can be a `caasette-deck`. The difference being what it is required to do in order for it to use a `caasette` (or `caasette-well`). Specifically a `caasette-deck` requires a global resolver method and requires a `process.env` variable to be set to be able to use `caasette`s. The reason for this is so that the `caasette` is able to reason about loading a `caasette` into the deck.

Specifically this code is required on the `window` oject:

```js
var process = {
  env: {
    NODE_ENV: 'development',

    // Your local Jukebox URL.
    BASE_JUKEBOX_URL: 'http://localhost:3000'
  }
}

var caasettePlayer = {
  addResolver: function (key, url) {
    caasettePlayer.resolvers[key] = url
  },
  resolverUrl: function (key) {
    return caasettePlayer.resolvers[key]
      ? caasettePlayer.resolvers[key]
      : process.env.BASE_JUKEBOX_URL
  },
  resolvers: {
    // Please convert this using webpack and on build.
    '@caasette': process.env.BASE_JUKEBOX_URL
  }
}
```

Note, this will change as the naming convention will change.

## Drawbacks
Complicated. It is required that the `@caasette` exports this functionality for `webpack` or `parcel` or directly in the `index.html` file.
