# Architecture
If you wouldn't use microservices for your small team then you wouldn't use `@caasette-player`. `@caasette-player` is for large teams that require the same app but want to have the freedom to build and deploy their `@caasette`s as they wish w/o interferance with other teams. Near complete autonomy.

## Summary
Using the [C4 Model](https://c4model.com) as a guideline I will show the architcture of a `@caasette-player` implimentation.

## Motivation
It is important to understand the architcture of a `@caasette-player` implimentation at a high level.

## Detailed design
This is an FE design. A `@caasette-player` implimentation works the same, basically, as any other JS module. The only difference is that it gets the component from the web. This isn't SSR because a `@caasette` is keyed into a component and retrieved from an API "somewhere" and retured as JS. This way we don't require React to interpret a [potentially] large string. JavaScript parsed directly is quicker.


![](../images/architecture/System-Context-Diagram.png)


In this container diagram we can see that a `@caasette-player` contains `@caasette-player/well`s that can contail any number of `@caasette-player/caasette`s. Each `@caasette-player/caasette` can then invoke a specific `@caasette-player/jukebox` to retrieve from their store and send back to the `@caasette-player/well` to render.

![](../images/architecture/Container-Diagram.png)

## How we teach this
There are some terms that people must understand and I will go into some detail about what they are and the expectation of use.

### `@caasette-player`
A `@caasette-player` is a definition of a system of module that will build, compose, retrieve and render a `@caasette`. It is not an implimentation of a `@caasette-player` which I think is where most of the confusion comes. A `@caasette-player` defines not just the process for making, retrieving and rendering a `@caasette` but it also defines, albeit loosely, a `@caasette-player/deck`, the jukebox and the containers for a `@caasette` called a well.

Any implimentation of a `@caasette-player` will include the deck, a jukebox and a well. A `@caasette-player` can also contains multiple decks.

### `@caasette-player/caasette`
A caasette is a module of the [UMD](https://github.com/umdjs/umd) spec that can be loaded and unloaded from a 

### `@caasette-player/well`
A well is an optional component that is defined in the `@caasette-player` namespace/org under `@caasette-player/well`. A well is a container that can hold a `@caasette-player/caasette`. It is a branching component that will render either a skeleton/loading state or the `@caasette` once it's available. It handles events (loaded, unloaded) and props propagation if required by a `@caasette`.

### `@caasette-player/jukebox`
The server that will return a `@caasette` to a `@caasette-player` implimentation.

### `@caasette-player/deck`
A `@caasette-player` implimentation.

## Drawbacks
If you wouldn't use microservices for your small team then you wouldn't use `@caasette-player`. `@caasette-player` is for large teams that require the same app but near complete autonomy. If, say, your team is 5 people then this is completely the wrong way to try to build your SPA.

The setup cost will be very large for a small team. Teaching your team how to build for a `@caasette-player` implimentation will also [possibly] be too large.

### v. Monolith
A monolith is great for swift startup and deploys. The problem is when your pages/routes et al start to get too large you have to think about how to split up code (chucking, splitting, vendor) in such a way to speed up downloads and sometimes executions. The bigger the teams, though, the slower the development and deployments. There is much more to be accountable for which makes companies antsy when it comes to pushing code out.

### v. SSR
SSR is great and should be used but if you need to be more dynamic and more autonomy then you need the flexibility of `@caasette-player`. I suggest using SSR for the main page for SEO and use `@caasette-player` for all other components.
