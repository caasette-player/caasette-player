# Caasette

## Summary
> A `caasette` is a component that aids in the pulling in of a remote component

## Motivation
I want to be able to load in remote components from "the cloud" without it being a string. Hopefully to speed things up.

## Detailed design

![](../images/caasette/Caasette.png)


## How we teach this
```jsx
import Caasette from '@caasette-player/loadable'

export default () => (
  <Caasette caasette="@caasette/ReactRectanglePopupMenu/Simple" />
)
```

## Drawbacks
Slow network/server.

Someone doesn't know how to build a `caasette` properly but we have `@caasette-player/blank-tape` & `@caasette-player/blank-deck` to aid in those endeavors.
