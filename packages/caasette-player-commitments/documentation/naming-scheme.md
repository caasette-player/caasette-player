<!-- # NPM Namespaces
The naming scheme for packages and their groupings

## caasette-player
`@caasette-player/${PACKAGE-NAME}`

## caasette-deck
`@caasette-deck/${PACKAGE-NAME}`

# A caasette
`@caasette/{$COMPANY-NAME}/${PACKAGE-NAME}/${NAMED-EXPORT}` -->

- Start Date: 2018-12-01

# Caasette Player Glossary (And relationships)

## Summary
An overview and description, glossary even, of all the terms/tech that embody `@caasette-player`. 

## Motivation
Adam Wolfe asked me to do this. So here we are.

## Glossary
<!-- * Caasette player
* Caasette deck
* Caasette [Loadable]
* Caasette Wells
* Caasette Splicer
* Jukebox
* Blank Tape
* Record Store
* Caasette Case Inlay -->

### Caasette player
A toolset and definition for caasette players. The Caasette Player is a spec for how to handle remote components using a service. The toolset are all the packes contained within this repo that allows for one to create your own Caasette Player (deck). 

The reasoning for 

### Caasette deck
An implimentation of a player (used interchangeably with the actual package)

### Caasette [Loadable]
A component that is requested from a service

### Caasette Wells
Where a caasette can be placed

### Caasette Splicer
Splitter

### Jukebox
A backend definition (used interchangeably with the actual package)

### Blank Tape
A CLI for making caasette repos

### Record Store
Your website

### Caasette Case Inlay
The package for local development of a component library, or something.
