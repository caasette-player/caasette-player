# CAASETTEs, UMDs & You

## Summary
A `caasette-well` is a definition of a container for `caasette`s that belongs only in a deck. (but it's a React component so you can really do w/e you want with it really).

## Motivation
I believe it is important for a deck to be able to handle `caasette`s and show a generalized skeleton state. A well is a branching container that can handle this and provide props and state to a `caasette`. So a `well` is also a provider.

## Detailed design
Would like to be able to handle multiple caasettes but it may not make sense so I prose two ways to handle it. One is a 1:1 relationship between the `well` and a `caasette` and the other is to enhance the `caasette` with a skeleton state and use that component within a `well` (which may provide further functionality still)

```js
// Option 1:
// Some well defined components go here:
const CustomCaasette = <Caasette name="@caasette/some/caasette/name" timeout={10} />
const Skeleton = <SkeletonState {...someProps} />

// Either render the skeleton state or the caasette.
<Well caasette={CustomCaasette} skeleton={Skeleton} />

// Option 2:
const Skeleton = <SkeletonState {...someProps} />
const CustomCaasette1 = enchanceWithSkeleton(<Caasette name="@caasette/some/caasette/name" timeout={10} />, Skeleton)
const CustomCaasette2 = enchanceWithSkeleton(<Caasette name="@caasette/some/caasette/name" timeout={10} />, Skeleton)
const CustomCaasette3 = enchanceWithSkeleton(<Caasette name="@caasette/some/caasette/name" timeout={10} />, Skeleton)

<Well>
  <CustomCaasette1 />
  <CustomCaasette2 />
  <CustomCaasette3 />
</Well>

```

## Drawbacks
omg all that typing ...
