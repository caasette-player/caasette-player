# Organizational Benefits

## Summary
I think it makes sense to explain the benefits to the organization and customers why using the `@caasette-player` definition will change the way you build front end software. And for the better.

## Motivation
There are two [difficult] problems that organizations want to solve when building software with multiple teams.
1. One big project
1. Multiple applications

### One big project
When there are multiple teams working on one big project what tends to happen are teams stepping on each others' toes/code at any part of the SDLC. A new branch, pushing, CI/CD, releasing. That requires so much work and awareness that in many big companies they use release officers to make sure code isn't injected by someone by mistake. Releases become slow and in some places they release once every 2 weeks, month, 2 months. That is not something any company should be doing.

### Multiple applications
Multiple applications is excellent for the autonomy of a team. The speed at which you develop, push and the CI/CD is completely controlled by the team. As it should be. The issue is that there are duplicated libraries of varying versions. If on the Front End this is awful for the user. They end up downloading, say, React multiple times slowing them down. Let's not do that.

## But how 2 ⚒️?
Simple. We adapt a CQRS platform but for the FE. It's overly simple though because there is only one command (get) and there is only one view (`@caasette`) and the event store is your `jukebox` that is mostly just a passthrough for the key it was passed.

