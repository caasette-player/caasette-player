# Organization Workflow

## Summary
How an organization should impliment and do their `caasette-player` stuff. Simplified cuz i ain't know you buddy

## Motivation

## Detailed design
### Caasette -> Prod environment
Once a `caasette` repo is pushed and a `caasette` is published then you're good to go (if you have your Jukebox setup locally or on a managed environment). 

### Building Documentation + Component Library
To build your site and documentation (which is important) you'll need to make a project for this. In the config file you add all your `caasette` repos. Then when ready it'll clone all your repos, install their deps then start building.

## How we teach this
Use the CLI tools or pull in `blank-tape` and get started. Learn how to build your site. That's it really.

## Drawbacks
There really shouldn't be any here. This is straightforward and simple. Building your site could be a little slow but that isn't important for Prod tho so you're good to go there.

## Alternatives
None, this is the first and best.

