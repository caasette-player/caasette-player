# Jukebox

## Summary
A `jukebox` the algorithm that can handle retrieving and serving up `caasette` tapes.

## Motivation

## Detailed design
A `jukebox` does 3 things.
1. Get a string request for a `caasette` ála `some/caasette`
1. Find that `caasette` from somewhere that maps to that key. e.g., DB store, flat file, NPM
1. Sends back that `caasette` as a JavaScript string.

This can all be done right in a single function but a `jukebox` implimentation is not limited to just resolving simple `caasette` requests.

A `jukebox` implimentation is not required to know how to resolve versioned requests.

## Drawbacks
Having to write server software. So one is provided already.
