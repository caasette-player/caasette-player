# Running your Guild

## Roles
1. "The most excellent illuminated and illustrious Czar of the Deck"
    - The person(s) involved in keeping the deck untainted
    - All communication must be in olde english when holding meetings.
    - Whenever addressed by anyone they must first say the full title "The most excellent illuminated and illustrious Czar of the Deck".
1. Everyone else
    - Everyone else.

## Rules
* Once you have build your Deck do not allow anyone to modify it or add to it
* A caasette can expect nothing from the deck (except: setup props in the manifest for stuff that already exists)

## Problems
* We require something that we believe is actually required for my caasette
* It will affect everyone else

## How to fix
* Write a proposal
* Bring it to the "The most excellent illuminated and illustrious Czar of the Deck" for any questions to be answered in proposal
    - "The most excellent illuminated and illustrious Czar of the Deck" will also reply in only old english.
* The "The most excellent illuminated and illustrious Czar of the Deck" will bring it to the guild for talking.
* Vote
