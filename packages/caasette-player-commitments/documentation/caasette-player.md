# The seemingly convoluted naming scheme of Caasette Player and why you're wrong about that. Champ.

## Summary
Hey, let's talk about the `caasette-player` definitions.

## Motivation
This sucks because i'm using word play to make definitions fall into place. It's also super fun for me. I'm sort of sorry about that but I also like my naming scheme and only god will make me change it. I'd be more sorry but I'm not. I, I, I just amn't.

* This project is called `caasette-player`
* This repo is named `caasette-player` to reflect the project.
* The org is `@caasette-player` to further drive that home.

A `caasette-player` is a grouping of tools and packages to build your own distributed front end softwares. It defines a `caasette-deck` which is a concrete implimentation of a `caasette-player`. Then there is the `@caasette-player/deck` which is our sample/example `caasette-deck` to be used.

Basically if I'm using `caasette-player` then I'm talking about abstract. I'm being philosophical. If I'm talking about `caasette-deck` then I'm talking about a specific `caasette-deck` that you've build (or I've built). `@caasette-player/deck` is an exact implimentation of that. I think it's pretty straight forward `caasette-player` -> `caasette-deck` -> `@caasette-player/deck`. But I also may be off my rocker at this point.

At least this isn't following some functional programming naming scheme. Or god forbid a Java naming scheme. You're welcome.

## Drawbacks
People be difficult(er than me).
