# (Title goes here)

## Summary
> One paragraph explanation of the feature.

## Motivation
> Why are we doing this? What use cases does it support? What is the expected outcome?

## Detailed design
> This is the bulk of the document.

> Explain the design in enough detail for somebody familiar with the framework to understand, and for somebody familiar with the implementation to implement. This should get into specifics and corner-cases, and include examples of how the feature is used. Any new terminology should be defined here.

## How we teach this

> What names and terminology work best for these concepts and why? How is this idea best presented? As a continuation of existing `@caasette-player` patterns, or as a wholly new one?

> Would the acceptance of this proposal mean the `@caasette-player` guides must be re-organized or altered? Does it change how `@caasette-player` is taught to new users at any level?

> How should this feature be introduced and taught to existing `@caasette-player` users?

## Drawbacks

> Why should we *not* do this? Please consider the impact on teaching `@caasette-player`, on the integration of this feature with other existing and planned features, on the impact of the API churn on existing apps, etc.

> There are tradeoffs to choosing any path, please attempt to identify them here.

## Alternatives

> What other designs have been considered? What is the impact of not doing this?

> This section could also include prior art, that is, how other frameworks in the same domain have solved this problem.

## Unresolved questions

> Optional, but suggested for first drafts. What parts of the design are still TBD?
