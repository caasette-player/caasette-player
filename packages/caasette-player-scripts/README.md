# `scripts`

> Until this eco system is ready it's going to be hacky. These scripts are my way to make my life easier. Don't judge.

This is only temporary until the alpha is finished

![](https://i.imgur.com/e2aOn6y.jpg)

## Usage

```
# Install scripts globally
npm i -g @caasette-player/scripts

# Download & Install
cd /path/to/top/level/directory
@cpd

# Open 1 terminal for watching changes
@cpw

# Open another terminal to start the deck app
@cps

```
