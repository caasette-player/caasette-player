# `@caasette-player/loadable`

> Wanna load dynamic React components from the "out there"? WELL HAVE I GOT SOME FANCY FUCKING NEWS FOR YOU!

![](https://i.imgur.com/XFJHjhD.jpg)

## Usage

In a JSX file
```js
// First bring it in
import Caasette from '@caasette-player/loadable'

// Then go ahead and get yourself a Caasette.
<Caasette caasette="@caasette/BlogPost/Simple" />

// Unfortunatly, for now, you still gotta get a couple of things.
```

Still need to figure this out but until then you'll need to put this onto the `window` object. I put mine in my `index.html` for now. Will consider making a webpack plugin for this. 
```js
// Should be put in here via WebPack.
var caasettePlayer = {
  addResolver: function (key, url) {
    caasettePlayer.resolvers[key] = url
  },
  resolverUrl: function (key) {
    return caasettePlayer.resolvers[key]
      ? caasettePlayer.resolvers[key]
      : process.env.BASE_JUKEBOX_URL
  },
  resolvers: {
    // Please convert this using webpack and on build.
    '@caasette': process.env.BASE_JUKEBOX_URL
  }
```