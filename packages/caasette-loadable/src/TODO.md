Make all this functional please, it's killing me.

Oh, and test this shiz.

* Resolver configuration with `@caasette` being the default going to "`jukebox`"
    - Currently running locally only
* A completed `caasette-loadable` module
    * Add/Remove `caasette`s
    * Loading components (set by `caasette-well` size)
* Exported `caasette-tape`
    * Build in the example repo
    * 3 examples
        - Form
        - [Fake] Gallery
        - [Fake] Blog
* `caasette-tape` loading nested `caasette-tape`
* Routes loading `caasette-tape`

List some of the other loaders' features here.
* https://github.com/jamiebuilds/react-loadable#------------api-docs
