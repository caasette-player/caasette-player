/* eslint-disable react/destructuring-assignment */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-trailing-spaces */
/* eslint-disable no-unused-vars */
/* eslint-disable semi */
/* eslint-disable comma-dangle */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { load } from './loader'

// @TODO: https://github.com/slorber/react-async-hook
// @TODO: Use React Hooks: https://reactjs.org/docs/hooks-intro.html
export default class Caasette extends Component {
  static propTypes = {
    caasette: PropTypes.string.isRequired
  }
  
  async componentDidMount() {
    this.Tape = await load(this.props.caasette)
    this.forceUpdate()
  }

  // We want this to be a proper HoC that "branches" based on props.
  // Do this via React Hooks.
  render = () => (
    typeof this.Tape === 'function'
      ? <this.Tape />
      : <img src="https://i.imgur.com/pKopwXp.gif" />
  )
}
