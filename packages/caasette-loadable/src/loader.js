/* eslint-disable no-console */
// https://github.com/eldargab/load-script
// https://css-tricks.com/thinking-async/
// https://stackoverflow.com/questions/7718935/load-scripts-asynchronously

/**
 * API
 * (Caasette.* doesn't mean this is a class just invocations of functions)
 *
 * Caasette.player()
 *   => This one is a question for me. Should I do this so that we can bootstrap
 *      the app or do it directly in the html?
 *
 * Caasette.components()
 *   => A list of caasettes currently being managed by Caasette Player.
 *      I may compose all these things to have access to an internal state
 *      instead of toplevel of this file for for now it isn't nessessary.
 *
 * Caasette.append('@caasette/requisition/{component-name}')
 *   => Attach/append a new caasette component
 *
 * Caasette.remove('@caasette/requisition/{component-name}')
 *   => Remove a caasette component and it's functionality (for Garbage Collection)
 *
 * Caasette.free()
 *   => Free all the caasettes.
 *
 * @TODO: Curry & Compose to have all state managed within a single function?
 */

// This won't be required once the system is built to have the deps. We'll make this DI instead. Hopefully. *crosses fingers*
// import waitForReact from './WaitForReact'
import { caasetteScript, caasetteMetaData } from './utils'
import { stdOnEnd } from './events'

// Get rid of these two variables.
// const global = window.global = window.global || { Remote_Components: {} }
// const Remote_Components = global.Remote_Components
const SORRY_IM_USING_WINDOW = window
const caasettes = {}

/**
 * for caasette in caasettes
 *    caasettes[caasette].parentNode.removeChild(caasettes[caasette])
 *    delete caasettes[id]
 *
 * delete caasettes
 */
export const free = () => { }

// @TODO: Get me to work correctly.
export const remove = (cassettte) => {
  const url = `http://localhost:3000/${cassettte}.js`

  // Remove the script tag from the DOM.
  caasettes[url].parentNode.removeChild(caasettes[url])

  // Remove the code from memory.
  delete caasettes[url]
}

export const append = (componentName, cb) => {
  const head = document.head || document.getElementsByTagName('head')[0]
  const script = caasetteScript(componentName, cb)

  // Please test in IE >=9, Firefox & Safari (plus the mobile browsers).
  stdOnEnd(script, cb)

  // @TODO: Do this elsewhere.
  head.appendChild(script)
}

// @TODO: Convert to monad.
const caasette = (componentName) => {
  const { url, cartridge, caasette } = caasetteMetaData(componentName)

  if (SORRY_IM_USING_WINDOW['@caasette'][cartridge][caasette]) {
    return SORRY_IM_USING_WINDOW['@caasette'][cartridge][caasette]
  }

  if (SORRY_IM_USING_WINDOW['@caasette'][cartridge].default) {
    return SORRY_IM_USING_WINDOW['@caasette'][cartridge].default
  }
}

// @TODO: convert to async await.
export const load = (componentName) => (
  new Promise((resolve, reject) => {
    const { url } = caasetteMetaData(componentName)

    // Check the cache.
    if (caasettes[url]) {
      resolve(caasettes[url])
      return
    }

    // Load the script.
    append(componentName, (err, script) => {
      if (err) {
        return reject(err)
      }

      const tape = caasette(componentName)

      if (tape) {
        caasettes[url] = tape
        resolve(tape)
      } else {
        reject(new Error(`Sorry, I could not load "${componentName}"`))
      }
    })
  })
)
