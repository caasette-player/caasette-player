const resolverUrl = window.caasettePlayer.resolverUrl

const PARTS = {
  RESOLVER_KEY: 0,
  CASE: 1,
  CAASETTE: 2,
}

// Get all the components.
export const components = () => {}
export const player = () => {}

// Return a new script DOM element.
export const caasetteScript = (componentName) => {
  const script = document.createElement('script')

  script.type = 'text/javascript'
  script.charset = 'utf8';
  script.async = true
  script.src = caasetteMetaData(componentName).url
  script.setAttribute('caasette', `@repo-manager/components/${componentName}`)

  return script
}

// Make functional.
export const caasetteMetaData = (caasetteName) => {
  const parts = caasetteName.split('/')
  const urlBase = resolverUrl(parts[PARTS.RESOLVER_KEY])

  return {
    caasette: parts[PARTS.CAASETTE],
    cartridge: parts[PARTS.CASE],
    url: `${urlBase}/${parts[PARTS.CASE]}.js`,
  }
}
