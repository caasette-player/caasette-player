/**
 * @TODO: Eventually this will be removed and put back into utils. It's here
 * for now until it is tested against other browsers to make sure it works as
 * intended; because if not then we have the problem of more code and moving it
 * into here will make all that extra code cleaner.
 */
// Onload && OnError callbacks -------------------------------------------------
// Unfortunatly this modifies the script object.
export const stdOnEnd = (script, cb) => {
  script.onload = function () {
    this.onerror = this.onload = null
    cb(null, script)
  }

  script.onerror = function () {
    // this.onload = null here is necessary
    // because even IE9 works not like others
    this.onerror = this.onload = null
    cb(new Error('Failed to load ' + this.src), script)
  }
}

// Currently unused. See below.
export const ieOnEnd = (script, cb) => {
  script.onreadystatechange = function () {
    if (this.readyState != 'complete' && this.readyState != 'loaded') return
    this.onreadystatechange = null
    cb(null, script) // there is no way to catch loading errors in IE8
  }
}

/**
 * The old code in loader.js after creating the script object:
 *
 * onend(script, cb)
 *
 * // some good legacy browsers (firefox) fail the 'in' detection above
 * // so as a fallback we always set onload
 * // old IE will ignore this and new IE will set onload
 * if (!script.onload) {
 *   stdOnEnd(script, cb);
 * }
 */
