In `@caasette-player/loadable` change the resolver from `@caasette` to `@jukebox` so as to make it clearer. Should also change the resolver method to reflect this.

In `@caasette-deck` I want to change the instances of `<Caasette />` to `<RemoteCaasette />`. People were having a hard time with it and I understand.
