__OLD README__

# Caasette Player
Microservices for the front end. (and a simple server for the BE)

# Stuff
## Front End
* `async` Module Loader
	- Possible interface:
		- `Player.load('@caasette/foo/bar')`
		- `Player.unload('@caasette/foo/bar')`
	- Singlton for shared state or all within the loader?
* Handles the Caasette Manifest
	- Version
	- Assets (images, css et al)
	- Split
* Logger
	- Analytics
	- Logging
* Main Library Bundle
	- Where we hold all the main stuff to be passed into each Caasette
		- `React`
		- `ReactDOM`
		- `React Router` or `Reach Router`
		- `Ramda`
		- `Recompose`
* Splitter (Think of better name please)
	- AB Test splitter (decide on components)
	- URL Splitter
* Configure backend URL if custom.

## Back End
Not an API for business logic. Store that elsewhere. Should it just take from NPM or is that a bad idea?

* Cache JS?
* Authentication?
	- Overkill? Dumb?
* Deliver Caasette & Assets and anything else required
* SSR
* Listener for new components?
	- When new ones are published?

## Developer
* CLI + `Yeoman`
	* Generate new monorepo of components (Record Label)
	* Generate new component in Record Label.
* `Learna`
* `Rollup` -> `UMD`
	- Eventually use `Babel` to convert it to a custom `caasette`
	- A `caasette` is basically a `UMD` component but it has some extra features.
		- Manifest included so we know more about it and it's settings
		- Where Images are stored if you care about that.
		- Is put into a CP namespace (`window.caasetteplayer['@caasette/foo/bar']`) `@see` below
			- Maybe, MAYBE version this but I don't like the idea (`'@caasette/foo/bar@1.0.3`). I don't believe the FE should be versioned.
* Auto generate versioned documentation
	- Defaults to latest
	- Keeps list of versions, somehow ...
		- Right in the repo I imagine
	- Puts content in a different repo possibly?
	- `Components`
		- Record Store
		- API
		- Guides
	- `Main`
		- API
		- Blog
		- Versions
		- Guides
* `Cypress` tests
  - Reads these tests to build out a page for business rules.

## Planned Packages
_Eat own dog food? Use a mono repo or have separate repos?_ I prefer the idea of multiple repos for this package honestly but using the packages to build a microservice type site.

* Site builder
* Babel plugin(s)
* Logger
* Remote component
* Backroom (BE)
* Remote Component
* Splitter
* Caasette Loader
* Record store
	- Like storybook but specific to this and cleaner.
* Backroom listener?
* Docker Microservices setup? What even is this?

# Risks
* Will promises, `async`/`await` get borked?
* Nested components
* Routing
* Getting all `async` stuff to work correctly and together.

---
### Preferable caasette export.
```js
window.caasetteplayer['@caasette/foo/bar'] = {
	// All named exports.
	exports: {
		// Dumb? Not sure. I don't think that UMD uses this.
		default: () => {},

		// Named exports.
		ComponentNameFoo: () => {},
		ComponentNameBar: () => {},
		ComponentNameBaz: () => {},
		ComponentNameQux: () => {},
	},
	manifest: {
		// The current version. Always the latest.
		version: '1.0.0',
		
		// Do not recycle. ie, keep in browser memory because it's important
		recycle: false,
		
		// the main state to inject into this component
		state: 'main',
		
		// Tells us if we are going to have nested remote components. Not sure this is useful.
		nested: true,
        
        // I don't see how this would work but I wanted it in here anyway. For ... reasons?
        props: {},
	}
}
```
