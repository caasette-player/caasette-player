# Caasette Player

Caasette Player is a set of packages to allow for the development of distribuated web interfaces (Components as a service ie: `caas`) where each interface (caasette tape) is built completely atonomously of any other all of which can be used within a single player. There are different types of players too. 

## Motivation
> I believe that building a component should be simple and fast.
> 
> I believe that building a component should have it's own sandbox.
> 
> I believe that configuration is the devil and more of it that I do not have to do the better.
> 
> I believe that documentation should be written inline and published for me.
> 
> I believe that documentation should be written wherever and however I like.
> 
> I believe that the system should not get in my way but I'm willing to deal with some issues.
> 
> I believe that I should have autonomy as to how I build my software.
> 
> I believe that disparate code should still be able to work together.
> 
> I believe that I should be able to compile my documentation together from many different repos.
> 
> I believe that my business rules should be easy to write and file away for later use.
> 
> I believe that writing tests should not get in my way.

These are all things that I believe are important to writing not only good components but good code. This, I believe, is a great way to achieve those end goals. Using `blank-tape` and the `caasette-player` tools we can build an eco-system to suit our needs and make it easy to build all our code.

## Packages
* [`caasette-babel-preset`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-babel-preset)
* [`caasette-eslint-preset`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-eslint-preset)
* [`eslint-plugin-caasette`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/eslint-plugin-caasette)
* [`rollup-plugin-caasette`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/rollup-plugin-caasette)
* [`caasette-loadable`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-loadable)
* [`caasette-wells`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-wells)
* [`docz-site-theme`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/docz-site-theme)
* [`cli`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/cli)
* [`caasette-player-scripts`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-scripts)
* [`logger`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/logger)
* [`splitter`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/splitter)
* [`blank-tape`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/blank-tape)
* [`caasette-player-commitments`](https://bitbucket.org/caasette-player/caasette-player/src/master/packages/caasette-player-commitments)
